import numpy as np
import cv2
from LineIterator import createLineIterator

DIR = "./Datasets/"
IMAGE = "0009.jpg"
HUE = [175, 185]            # red
# HUE = [55, 65]            # green
SEG_THRESHOLD = 30          # lower bound of color segmentation
DIST_THRESHOLD = 15         # how far can last point be from hypothesis (euclidean distance)
DIR_THRESHOLD = 45          # offset for generating good candidates (degrees)
DEG_THRESHOLD = 30          # maximal valid offset from 90 degrees in detected rectangle
EDGE_THRESHOLD = 0.05       # how "edgy" must the edge be
PATCH_THRESHOLD = 20        # minimal patch size


class Patch(object):
    """
    Class containing all pixels of detected corner and its properties
    """
    def __init__(self, pixels, img_grad_x, img_grad_y, patch_id):
        assert np.shape(pixels)[0] == 2
        self.pixels = pixels
        self.direction = self._compute_dir(pixels, img_grad_x, img_grad_y)
        self.id = patch_id
        self.position = np.mean(pixels, axis=1).astype(int)

    def _compute_dir(self, pixels, img_grad_x, img_grad_y):
        """
        calculate direction of corner using gradient
        """
        x = np.mean(img_grad_x[pixels])
        y = np.mean(img_grad_y[pixels])
        return np.degrees(np.arctan2(y, x)) + 180  # return degree from 0 - 360

    def recalculate_position(self, grad):
        """
        push position of corner in the direction of gradient, to the corner of this patch
        """
        tmp = np.sum((np.asarray(self.pixels).T * np.asarray(grad)), axis=1)
        idx = np.argmin(tmp)
        self.position = ((np.mean(self.pixels, axis=1).astype(int)
                          + 2*np.asarray(self.pixels)[:, idx.item()])/3).astype(int)


class Segment(object):
    """
    Class containing all corner patches assigned to one segment
    """
    def __init__(self, segment_id):
        self.segment_id = segment_id
        self.patches = []

    def add_patch(self, patch):
        self.patches.append(patch)

    def create_hypothesis(self, canny):
        """
        This method creates hypothesis about the top side of brick. It iterates possible combination of corners
        in this segment and determines whether certain combination is rectangle we are looking for. This rectangle
        should have all corners with gradient pointing inside the rectangle (in color segmented image). Corners also
        must have edges between themselves, this is ensured using line iterator over canny image and summing the values.
        And of course all corners should have ~90 degrees.
        """
        # loop over all potential corners
        for idx, p in enumerate(self.patches):
            ret = []
            # get first corner
            p_dirs = self._filter_patches_direction(p.direction + 90, DIR_THRESHOLD)  # discard some corners
            for p_dir in p_dirs:
                if self._is_edge(p, p_dir, canny, EDGE_THRESHOLD):  # confirm edge using canny
                    ret.append(p_dir)
                    break
            if len(ret) == 0:
                # if not corner skip to next patch
                continue
            # push Patch.position away from center of rectangle
            vec = p.position - ret[0].position
            grad = vec + np.asarray([-vec[1], vec[0]])
            ret[0].recalculate_position(grad)

            # get second corner
            p_dirs = self._filter_patches_direction(p.direction - 90, DIR_THRESHOLD)
            for p_dir in p_dirs:
                if self._is_edge(p, p_dir, canny, EDGE_THRESHOLD):
                    ret.append(p_dir)
                    break
            if len(ret) == 1:
                continue
            # push corner position away from center of rectangle
            ret[1].recalculate_position(-grad)

            # get last corner of rectangle and complete the hypothesis
            vec1 = ret[0].position - p.position
            vec2 = ret[1].position - p.position
            p.recalculate_position([-grad[1], grad[0]])
            if abs(np.degrees(np.arctan2(vec1[1], vec1[0]) - np.arctan2(vec2[1], vec2[0])) - 90) > DEG_THRESHOLD:
                continue
            expected_pos = p.position + vec1 + vec2
            p_poss = self._filter_patches_position(expected_pos, DIST_THRESHOLD)
            for p_pos in p_poss:
                if self._is_edge(ret[0], p_pos, canny, EDGE_THRESHOLD) and \
                        self._is_edge(ret[1], p_pos, canny, EDGE_THRESHOLD):
                    p_pos.recalculate_position([grad[1], -grad[0]])
                    ret.append(p_pos)
                    ret.append(p)
                    return ret
        return None

    def _filter_patches_position(self, pos, thr):
        """
        return patches close to the position in argument
        """
        ret = []
        for p in self.patches:
            if (np.linalg.norm(pos - p.position)) < thr:
                ret.append(p)
        ret.sort(key=lambda x: np.linalg.norm(pos - x.position))
        return ret

    def _filter_patches_direction(self, direction, thr):
        """
        return patches with direction in argument
        """
        ret = []
        incr = 360 if direction < 180 else -360
        for p in self.patches:
            if min(abs(direction - p.direction), abs(direction + incr - p.direction)) < thr:
                ret.append(p)
        ret.sort(key=lambda x: min(abs(direction - x.direction), abs(direction + incr - x.direction)))
        return ret

    def _is_edge(self, patch1, patch2, canny, thr):
        """
        resolves if corner combination is edge using canny
        """
        pt1 = np.asarray((patch1.position[1], patch1.position[0]))
        pt2 = np.asarray((patch2.position[1], patch2.position[0]))
        line = createLineIterator(pt1, pt2, canny)
        line_vals = line[:, 2]
        val = np.sum(line_vals)/255
        if val > thr*len(line_vals):
            return True
        else:
            return False


def segment_color(rgb_img, hue):
    """
    this segmentation method works very well in gazebo but I think that performance will be poor in real world.
    TODO: adjust segmentation (use hue and brightness instead of brightness and saturation)
    """
    hsv = cv2.cvtColor(rgb_img, cv2.COLOR_BGR2HSV)

    # grab the image dimensions
    h = hsv.shape[0]
    w = hsv.shape[1]
    ret = np.zeros((h, w))
    if hue[0] < 90:
        hue_low = hue
        hue_up = [hue[0] + 180, hue[1] + 180]
    else:
        hue_low = [hue[0] - 180, hue[1] - 180]
        hue_up = hue

    # loop over the image, pixel by pixel
    for y in range(0, h):
        for x in range(0, w):
            # threshold the pixel
            if hue_low[0] < hsv[y, x, 0] < hue_low[1] or hue_up[0] < hsv[y, x, 0] < hue_up[1]:
                ret[y, x] = (1.0 * hsv[y, x, 1] * hsv[y, x, 2]) / 255.0
            else:
                ret[y, x] = 0.0

    return ret.astype(np.uint8)


def process_scene(img):
    # segment image
    segmented = segment_color(img, HUE)

    # extract edges
    edges = cv2.Canny(segmented, 100, 200)  # 255 for edge

    # smoothing
    segmented = cv2.GaussianBlur(segmented, (5, 5), 9)
    edges = cv2.GaussianBlur(edges, (5, 5), 9)

    # detect corners using harris
    dst = cv2.cornerHarris(segmented, 4, 3, 0.04)
    dst = cv2.dilate(dst, None)
    dst[dst > 0.01*dst.max()] = 255
    dst[dst < 0.01*dst.max()] = 0
    dst = dst.astype(np.uint8)

    # show harris and canny
    # img[edges > 50] = (255, 255, 255)
    # img[dst == 255] = (0, 0, 0)

    # create corner patches
    ret_cors, cors_markers = cv2.connectedComponents(dst)

    # create color segments
    segmented[segmented < SEG_THRESHOLD] = 0
    segments_num, segments_markers = cv2.connectedComponents(segmented)

    # compute gradients
    grad_x = cv2.Sobel(segmented, cv2.CV_64F, 1, 0, ksize=5)
    grad_y = cv2.Sobel(segmented, cv2.CV_64F, 0, 1, ksize=5)

    # assign corner patches to color segments
    segments = []
    for i in range(1, segments_num + 1):
        patch_ids = np.unique(cors_markers[segments_markers == i])[1:]
        if 4 <= len(patch_ids) < 50:
            seg = Segment(i)
            for j in range(len(patch_ids)):
                id = patch_ids[j]
                patch_pixels = np.where(cors_markers == id)
                if len(patch_pixels[0]) > PATCH_THRESHOLD:
                    pat = Patch(patch_pixels, grad_x, grad_y, id)
                    seg.add_patch(pat)
            segments.append(seg)

    ret = []
    order = [0, 3, 1, 2]  # we want to draw bounding box so we have to reorder corners
    for seg in segments:
        print("--- next segment ---")
        patches = seg.create_hypothesis(edges)
        # draw lines
        if patches is not None:
            print("--- brick found ---")
            patches = [patches[i] for i in order]
            ret.append(patches)

    return ret


if __name__ == '__main__':

    # read image
    img = cv2.imread(DIR + IMAGE, 1)

    # process
    bbs = process_scene(img)

    for bb in bbs:
        for i in range(4):
            c1 = (bb[i].position[1], bb[i].position[0])
            c2 = (bb[(i+1) % 4].position[1], bb[(i+1) % 4].position[0])
            cv2.line(img, c1, c2, (255, 0, 0), 3)

    cv2.imshow('image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
